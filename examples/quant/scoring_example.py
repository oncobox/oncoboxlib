from pathlib import Path

import pandas as pd

from oncoboxlib.quant.database import adjust_samples_for_db, load_database
from oncoboxlib.quant.scoring import calc_pal, calc_cnr, get_samples_columns_type


SANDBOX_PATH = Path(__file__).absolute().parents[2] / 'sandbox'


def main():
    db = load_database(SANDBOX_PATH / 'databases/Biocarta 1.123', 'csv')

    # load samples included norms
    samples = pd.read_csv(SANDBOX_PATH / 'cyramza_normalized_counts.txt', index_col='SYMBOL', sep='\t')

    # get norms columns names
    norm_columns, _ = get_samples_columns_type(samples, norms_included=True)

    # get case-to-norm-ratio (cnr)
    cnr = calc_cnr(samples, norm_columns)

    # Make samples file compatible with database:
    # remove missing genes, add genes from database, fill NaNs with na_fill_value.
    adjusted_cnr = adjust_samples_for_db(cnr, db, 1.0)

    # calculate scores
    pal = calc_pal(adjusted_cnr, db.arr, db.gp)

    # dump result
    pal.to_csv(SANDBOX_PATH / 'pal.csv')


if __name__ == '__main__':
    main()
