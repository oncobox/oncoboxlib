# Changelog

## Unreleased

Empty.

## 1.2.0 - 2021-03-02

- New option to add to result p-values for unequal variance t-test aka Welch's t-test.
- New option to add to result p-values corrected for FDR using Benjamini/Hochberg method.


## 1.1.0 - 2020-12-29
