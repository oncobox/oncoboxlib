import pytest

import numpy as np
import pandas as pd

from oncoboxlib.quant.scoring.samples import get_samples_columns_type, calc_cnr, add_external_norms


class TestGetSamplesColumnsType:

    def test_basic(self):
        samples = pd.DataFrame(
            columns=['SYMBOL', 'Sample_Tumour', 'sample2_tumor', 'sample2_CaSe', 'Norm', 'sample_norm'],
            data=[
                ['g1.symarr01.1', 2, 12, 17, 3, 5],
                ['g2.symarr01.1', 3, 4, 2, 2, 3],
                ['g1.symarr02.1', 12, 7, 9, 10, 9],
                ['g2.symarr02.1', 25, 3, 1, 10, 2]
            ]
        ).set_index('SYMBOL')

        # to split columns of valid file
        norms, cases = get_samples_columns_type(samples, norms_included=True)
        assert norms == ['Norm', 'sample_norm'], 'Wrong norms.'
        assert cases == ['Sample_Tumour', 'sample2_tumor', 'sample2_CaSe'], 'Wrong cases.'

        # all columns is cases when norms not included
        norms, cases = get_samples_columns_type(samples, norms_included=False)
        assert norms == [], 'Wrong norms.'
        assert cases == ['Sample_Tumour', 'sample2_tumor', 'sample2_CaSe', 'Norm', 'sample_norm'], 'Wrong cases.'

    def test_ambiguous_name(self):
        # raises exception when norms_included=True and one or more columns have ambiguous name

        samples = pd.DataFrame(
            columns=['Sample_Tumour', 'sample2_tumor', 'sample2_CaSe', 'Norm_case', 'tumor_norm'],
            data=[[2, 12, 17, 3, 5]]
        )
        with pytest.raises(ValueError):
            get_samples_columns_type(samples, norms_included=True)

    def test_ambiguous_name_and_unspecified_name(self):
        samples = pd.DataFrame(
            columns=['both_norm_and_case', 'unspecified'],
            data=[[1, 2]]
        )
        with pytest.raises(ValueError):
            get_samples_columns_type(samples, norms_included=True)


def test_calc_cnr():

    data = {
        'columns': ['SYMBOL', 'Sample_Tumour', 'Norm'],
        'data': [
            ['g1.symarr01.1', 2, 3],
            ['g2.symarr01.1', 3, 2],
            ['g1.symarr02.1', 12, 10],
            ['g2.symarr02.1', 25, 10]
        ]
    }

    samples = pd.DataFrame(columns=data['columns'], data=data['data']).set_index('SYMBOL')
    norms, cases = get_samples_columns_type(samples, norms_included=True)
    cnr = calc_cnr(samples, norms, 'gmean')

    expected_data = {
        'columns': ['SYMBOL', 'Sample_Tumour', 'Norm'],
        'data': [
            ['g1.symarr01.1', 0.6666666666666665, 0.9999999999999999],
            ['g2.symarr01.1', 1.5, 1.0],
            ['g1.symarr02.1', 1.1999999999999997, 0.9999999999999998],
            ['g2.symarr02.1', 2.4999999999999996, 0.9999999999999998]
        ]
    }

    expected = pd.DataFrame(columns=expected_data['columns'], data=expected_data['data']).set_index('SYMBOL')

    assert np.allclose(cnr[cnr.columns], expected[expected.columns], rtol=1e-16, atol=1e-16), "Wrong cnr."
    assert list(cnr.index) == list(expected.index), "Wrong cnr."
    assert list(cnr.columns) == list(expected.columns), "Wrong cnr."


def test_add_external_norms_quantile_method():

    samples_data = {
        'columns': ['SYMBOL', 'Sample_Tumour'],
        'data': [
            ['g1.symarr01.1', 2],
            ['g2.symarr01.1', 3],
            ['g1.symarr02.1', 12],
            ['g2.symarr02.1', 25]
        ]
    }

    norms_data = {
        'columns': ['SYMBOL', 'Colon'],
        'data': [
            ['g1.symarr01.1', 3],
            ['g2.symarr01.1', 2],
            ['g1.symarr02.1', 10],
        ]
    }

    samples = pd.DataFrame(
        columns=samples_data['columns'], data=samples_data['data']).set_index('SYMBOL')
    norms = pd.DataFrame(
        columns=norms_data['columns'], data=norms_data['data']).set_index('SYMBOL')
    samples_with_norms, norm_columns = add_external_norms(samples, norms, 'ngs counts')

    expected_data = {
        'columns': ['SYMBOL', 'Sample_Tumour', 'Colon'],
        'data': [
            ['g1.symarr01.1', 3, 4],
            ['g2.symarr01.1', 4, 3],
            ['g1.symarr02.1', 12, 12]
        ]
    }

    expected = pd.DataFrame(columns=expected_data['columns'], data=expected_data['data']).set_index('SYMBOL')

    assert np.allclose(
        samples_with_norms[samples_with_norms.columns], expected[expected.columns], rtol=1e-16, atol=1e-16
    ), "Wrong samples."
    assert list(samples_with_norms.index) == list(expected.index), "Wrong samples."
    assert list(samples_with_norms.columns) == list(expected.columns), "Wrong samples."
    assert norm_columns == ['Colon'], "Wrong norm columns."


def test_symbol_column():

    samples_data = {
        'columns': ['NOT_SYMBOL', 'Sample_Tumour'],
        'data': [
            ['g1.symarr01.1', 2],
            ['g2.symarr01.1', 3],
            ['g1.symarr02.1', 12],
            ['g2.symarr02.1', 25]
        ]
    }

    norms_data = {
        'columns': ['NOT_SYMBOL', 'Colon'],
        'data': [
            ['g1.symarr01.1', 3],
            ['g2.symarr01.1', 2],
            ['g1.symarr02.1', 10],
        ]
    }

    cases_df = pd.DataFrame(
        columns=samples_data['columns'], data=samples_data['data']).set_index('NOT_SYMBOL')
    norms_df = pd.DataFrame(
        columns=norms_data['columns'], data=norms_data['data']).set_index('NOT_SYMBOL')

    with pytest.raises(
        ValueError,
        match='Norms must have first column with name "SYMBOL" that contains genes names'
    ):
        _, _ = add_external_norms(cases_df, norms_df, 'ngs counts')

