import pandas as pd
from pandas.testing import assert_frame_equal

from oncoboxlib.common.math.stat import quantile_normalization


class TestQuantileNormalization:

    def test_empty(self):
        df = pd.DataFrame()
        qn_df = quantile_normalization(df)
        assert_frame_equal(qn_df, pd.DataFrame())

    def test_simple(self):
        df = pd.DataFrame([[1]])
        qn_df = quantile_normalization(df)
        assert_frame_equal(qn_df, pd.DataFrame([[1]]))

    def test_wikipedia_example(self):
        df = pd.DataFrame(
            [
                [5, 4, 3],
                [2, 1, 4],
                [3, 4, 6],
                [4, 2, 8],
            ],
            index=['A', 'B', 'C', 'D'], columns=['C0', 'C1', 'C2'])

        qn_df = quantile_normalization(df)

        expected = pd.DataFrame(
            [
                [5.666667, 4.666667, 2.000000],
                [2.000000, 2.000000, 3.000000],
                [3.000000, 4.666667, 4.666667],
                [4.666667, 3.000000, 5.666667],
            ],
            index=['A', 'B', 'C', 'D'], columns=['C0', 'C1', 'C2'])

        assert_frame_equal(qn_df, expected)
