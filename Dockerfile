# build:  docker build -t lib_ob .
# run for dev localy: docker run --rm -v /path/to/local/src:/src lib_ob python3 /src/examples/quant/scoring_example.py
FROM ubuntu:20.04

ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive && \
    apt-get install --no-install-recommends -yq apt-utils gcc \
    python3-dev python3-pip pkg-config \
    libpq-dev libgraphviz-dev graphviz \
    libpcre3 libpcre3-dev

# set the working directory in the container
WORKDIR /src
ENV PYTHONPATH="/src"

# copy the dependencies file to the working directory
COPY requirements/prod.txt requirements.txt

# install dependencies
RUN python3 -m pip install -r requirements.txt
